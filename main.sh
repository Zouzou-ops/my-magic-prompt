#!/bin/bash
rootpass=123

cmd() {

  while [ 1 ]; do

  read -p "Type a command: " args 
  tArgs=($args)
  cmd="${tArgs[0]}" 
  unset tArgs[0]

    case "$cmd" in 
      version  | --v | vers ) version;;	
      about ) about;;
      cd ) cd2;;  
      quit ) exit;;
      help ) help;;
      ls ) ls2;;
      pwd ) pwd2;;
      age ) age;;
      passw ) passw;;
      httpget ) httpget;; 
      rmd ) rmd2 ${tArgs[1]};;
      rm ) rm2 ${tArgs[1]};;
      profile ) profile;;
      hour ) hour;;
      open ) vim2;;
      ssmtp ) ssmtp2;;
      * ) echo -e "\e[91mCommand Invalid \e[0m";;
    esac
  done
}


main() {
    echo -n "Login : "
    read login
    echo -n "Password : "
    read -s password

    echo ${login} ${password}

    if [ "$login" != "zouzou" ] || [ "$password" != "$rootpass" ]; then
        echo "Invalid login or password"

        exit
    fi

    cmd
}

main