# Welcome to My Magic Prompt!
Prompt has multiple commands :

**help** : which will indicate the commands you can use
**ls** : list files and folders visible as hidden
**rm** : delete a file
**rmd** : delete a folder
**about** : a description of your program
**version** : displays the version of your prompt
**age** : asks your age and tells you if you are an adult or a minor
**quit** : allows you to exit the prompt
**profile** : allows you to display all the information about yourself.
First Name, Last name, age, email
**passw** : allows you to change the password with a confirmation request
**cd** : go to a folder you have just created or go back to a previous folder 
previous folder
**pwd** : indicates the current directory
**hour** : allows to give the current time
(*) : indicate an unknown command
**httpget** : allows you to download the html source code of a web page and save it in a specific file. Your prompt must ask you what the file name will be.
**smtp** : allows you to send a mail with an address, a subject and the body of the mail
**open** : open a file directly in the VIM editor even if the file does not exist
